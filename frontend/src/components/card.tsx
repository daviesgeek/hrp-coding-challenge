import React from 'react';
import 'tachyons';

interface IProps {
  title: string;
  content: string;
}

export function Card({ title, content }: IProps) {
  return (
    <>
      <article className="ba w-25 ma2">
        <h1 className="f4 bg-near-black white mv0 pv2 ph3">{title}</h1>
        <div className="pa3 bt">
          <p className="f6 f5-ns lh-copy measure mv0">{content}</p>
        </div>
      </article>
    </>
  );
}
