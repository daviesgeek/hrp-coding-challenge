import React from 'react';
import { render, screen } from '@testing-library/react';
import { Card } from './card';

test('renders card app with title & content', () => {
  render(<Card title="Test title" content="HRP Coding Challenge" />);
  const titleElement = screen.getByText(/Test Title/i);
  const contentElement = screen.getByText(/HRP Coding Challenge/i);
  expect(titleElement).toBeInTheDocument();
  expect(contentElement).toBeInTheDocument();
});
