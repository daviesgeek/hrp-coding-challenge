import React, { ChangeEvent, useEffect, useState } from 'react';
import 'tachyons';
import axios from 'axios';

import { Card } from './components/card';

let currentIndex = 1;

function App() {
  const [mean, setMean] = useState(0);
  const [median, setMedian] = useState(0);
  const [stdDeviation, setStdDeviation] = useState(0);
  const [mode, setMode] = useState<number[]>([]);

  const [input, setInput] = useState(0);

  useEffect(fetchData, []);

  // This is the core data fetching function. It's comprised of four main areas: mean, median, std dev, & mode
  function fetchData() {
    axios(`/api/data/${currentIndex}`)
      .then((r) => r.data.data)
      .then((data) => {
        // will use mean later, so store the result here
        const mean =
          data.reduce((acc: number, cur: number) => cur + acc, 0) / data.length;
        setMean(mean);

        // Median
        setMedian(
          data.length % 2 === 1 // odd or even
            ? data[Math.floor(data.length / 2)] // if odd, find the middle of the array
            : (data[data.length / 2] + data[data.length / 2] - 1) / 2 // if even, grab the middle two, add, & divide
        );

        // Standard deviation is a little more in depth
        setStdDeviation(
          Math.sqrt(
            data
              .map((v: number) => {
                const diff = v - mean;
                return diff * diff;
              })
              .reduce((acc: number, cur: number) => cur + acc, 0) / data.length
          )
        );

        // Mode is even more complicated
        const sortedData = data.sort().reverse();
        let diffs: number[] = [];
        sortedData.forEach((e: number, i: number) => {
          if (i === 0) return;
          diffs.push(e - sortedData[i - 1]);
        });

        let numbers: { [key: number]: number } = {};
        for (let i = 0; i < diffs.length; i++) {
          const element = diffs[i];
          if (element !== 0) continue;

          if (!numbers[sortedData[i]]) numbers[sortedData[i]] = 0;
          numbers[sortedData[i]]++;
        }

        // supports bi/multi modal
        let highestNumbers: number[] = [];
        for (const key in numbers) {
          let highestNumber = highestNumbers[0];
          if (
            numbers[key] > numbers[highestNumber] ||
            !numbers[highestNumber]
          ) {
            highestNumbers = [parseInt(key)];
          } else if (numbers[key] === numbers[highestNumber]) {
            highestNumbers.push(parseInt(key));
          }
        }

        setMode(highestNumbers);
      });
  }

  function onChangeInput(e: ChangeEvent<HTMLInputElement>) {
    setInput(parseInt(e.currentTarget.value, 10));
  }

  function onClickSave() {
    setInput(0);
    axios.post(`/api/data/${currentIndex}`, { input }).then(fetchData);
  }

  function onClickNext() {
    axios(`/api/data/${currentIndex}/next`).then((r) => {
      currentIndex = r.data;
      return fetchData();
    });
  }

  return (
    <div className="mh3">
      <h1>HRP Coding Challenge</h1>

      <input
        className="ml2"
        type="number"
        onChange={onChangeInput}
        value={input}
      />
      <button onClick={onClickSave}>Save</button>
      <button className="ml3" onClick={onClickNext}>
        Load Next Dataset
      </button>
      <span className="ml2">Dataset loaded: #{currentIndex}</span>

      <div className="flex">
        <Card title="Mean" content={mean.toString()}></Card>
        <Card title="Median" content={median.toString()}></Card>
        <Card
          title="Standard Deviation"
          content={stdDeviation.toString()}
        ></Card>
        <Card
          title="Mode"
          content={mode.length ? mode.toString() : 'None'}
        ></Card>
      </div>
    </div>
  );
}

export default App;
