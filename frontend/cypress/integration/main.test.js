describe('main tests', () => {
  beforeEach(() => {
    cy.server({force404: true})
    cy.visit('http://localhost:3000')
  })

  it('should pick up data from the server', () => {
    cy.route('/api/data/1', {data: [1,2,3]}).as('data')
    cy.wait('@data')

    cy.contains('Mean').next().should('have.text', '2')
    cy.contains('Median').next().should('have.text', '2')
    cy.contains('Standard Deviation').next().should('have.text', '0.816496580927726')
    cy.contains('Mode').next().should('have.text', 'None')
    cy.contains('Dataset loaded: #1').should('exist')
  })

  it('should cycle through datasets', () => {
    cy.route('/api/data/1', {data: [1,2,3]}).as('data1')
    cy.wait('@data1')

    cy.route('/api/data/1/next', 2).as('next1')
    cy.route('/api/data/2', {data: [5,6,7]}).as('data2')

    cy.contains('Load Next Dataset').click()

    cy.wait(['@next1', '@data2'])

    cy.contains('Dataset loaded: #2').should('exist')


    cy.route('/api/data/2/next', 3).as('next2')
    cy.route('/api/data/3', {data: [5,6,7,5]}).as('data3')

    cy.contains('Load Next Dataset').click()

    cy.wait(['@next2', '@data3'])

    cy.contains('Dataset loaded: #3').should('exist')


  })

  it('should calculate the mean', () => {
    cy.route('/api/data/1', {data: [1, 2, 2, 3, 3, 4, 7, 9],}).as('data')
    cy.wait('@data')

    cy.contains('Mean').next().should('have.text', '3.875')
  })

  it('should calculate the median', () => {
    cy.route('/api/data/1', {data: [1, 2, 2, 3, 3, 4, 7],}).as('data')
    cy.wait('@data')

    cy.contains('Median').next().should('have.text', '3')
  })

  it('should calculate the median with two middle numbers', () => {
    cy.route('/api/data/1', {data: [1, 2, 3, 4],}).as('data')
    cy.wait('@data')

    cy.contains('Median').next().should('have.text', '2.5')
  })

  it('should calculate the standard deviation', () => {
    cy.route('/api/data/1', {data: [1, 2, 2, 3, 3, 4, 7, 9],}).as('data')
    cy.wait('@data')

    cy.contains('Standard Deviation').next().should('have.text', '2.5708704751503917')
  })

  it('should calculate a bi modal', () => {
    cy.route('/api/data/1', {data: [1, 2, 2, 3, 3, 4, 7, 9]}).as('data')
    cy.wait('@data')

    cy.contains('Mode').next().should('have.text', '2,3')
  })

  it('should calculate a multi modal', () => {
    cy.route('/api/data/1', {data: [1, 2, 2, 3, 3, 4, 4, 7, 9]}).as('data')
    cy.wait('@data')

    cy.contains('Mode').next().should('have.text', '2,3,4')
  })

  it('should display none if no mode', () => {
    cy.route('/api/data/1', {data: [1, 2, 3]}).as('data')
    cy.wait('@data')

    cy.contains('Mode').next().should('have.text', 'None')
  })

  it('should save data to the server and update the dashboard with new data', () => {
    cy.route('/api/data/1', {data: [1, 2, 3]}).as('data')
    cy.wait('@data')
    cy.get('input').clear().type('1')

    cy.route('POST', '/api/data/1', {}).as('post')
    cy.route('/api/data/1', {data: [1, 2, 3, 1]}).as('data')
    cy.contains('Save').click()
    cy.wait('@post')

    cy.get('input').should('have.value', '0')
    cy.contains('Mean').next().should('have.text', '1.75')
    cy.contains('Median').next().should('have.text', '2.5')
    cy.contains('Standard Deviation').next().should('have.text', '0.82915619758885')
    cy.contains('Mode').next().should('have.text', '1')
  })

})