# HRP Coding Challenge

## Summary
A project for the coding challenge part of the HRP interview. 
A big important side note: there may be some issues with TypeScript checking inside this project due to some breaking changes in create react app. The project should compile and run properly regardless of said errors inside your IDE. More info [here](https://stackoverflow.com/a/65011963).
This project is built with [Hapi](https://hapi.dev/) on the backend and [React](https://reactjs.org/) using [create-react-app](https://github.com/facebook/create-react-app) to bootstrap it.

## Usage

```bash
cd frontend
yarn
yarn start # or npm start

# In another terminal
cd backend
yarn
yarn start # or npm start
```

## Testing

Backend unit tests:

```bash
cd backend
yarn test
```

Frontend unit tests:

```bash
cd frontend
yarn test:unit
```

Frontend e2e tests:

```bash
cd frontend
yarn start

# In another tab
yarn test:e2e
```

## Technologies/frameworks/tools
 - [Node.js](https://nodejs.org/en/)
 - [Yarn](https://yarnpkg.com/)
 - [Hapi](https://hapi.dev/)
 - [React](https://reactjs.org/)
 - [Tachyons](https://tachyons.io/)
 - [Cypress](https://www.cypress.io/)

## Conclusion
Thank you for time in reviewing this codebase. I appreciate the consideration and look forward to talking soon!

Matthew Davies