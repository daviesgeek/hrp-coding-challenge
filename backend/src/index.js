/**
 * This just bootstraps the server, the actual server + routes is in ./server.js
 */

'use strict';

const init = async () => {
  const server = require('./server');

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
