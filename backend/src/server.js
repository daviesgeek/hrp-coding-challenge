const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');

const server = Hapi.server({
  port: 3001,
  host: 'localhost',
});

// These are the three datasets you can cycle through
let datasets = [
  [1, 2, 2, 3, 3, 4, 7, 9],
  [1, 2, 3],
  [46, 47, 19, 9, 48],
];

// Super simple get request
server.route({
  method: 'GET',
  path: '/api/data/{index}',
  handler: (request) => {
    let data = datasets[request.params.index - 1]; // make it 1-indexed instead of zero

    if (!data) return Boom.notFound('missing');

    return { data };
  },
});

// This requests the next index for the dataset
//  it returns
server.route({
  method: 'GET',
  path: '/api/data/{index}/next',
  handler: (request) => {
    let data = datasets[request.params.index];
    if (!data) {
      return 1;
    } else {
      return parseInt(request.params.index) + 1;
    }
  },
});

// This adds new data to the dataset
server.route({
  method: 'POST',
  path: '/api/data/{index}',
  options: {
    validate: {
      payload: Joi.object({
        input: Joi.number().integer().required(),
      }),
    },
  },
  handler: (request) => {
    let data = datasets[request.params.index - 1];

    if (!data) return Boom.notFound('missing');

    data.push(request.payload.input);
    return { data };
  },
});

module.exports = server;
