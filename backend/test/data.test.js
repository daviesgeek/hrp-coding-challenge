'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const server = require('../src/server');

describe('/data', () => {
  beforeEach(async () => {
    await server.initialize();
  });

  afterEach(async () => {
    await server.stop();
  });

  describe('get', () => {
    it('should get the first dataset', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/api/data/1',
      });
      expect(res.result.data.length).to.equal(8);
    });
    it('should return 404 if the dataset is out of bounds', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/api/data/42',
      });
      expect(res.statusCode).to.equal(404);
    });
  });

  describe('post', () => {
    it('should post a new item to the dataset', async () => {
      const res = await server.inject({
        method: 'post',
        url: '/api/data/1',
        payload: 17,
      });
      expect(res.result.data.length).to.equal(9);
    });

    it('should post a new item to the dataset handling 404', async () => {
      const res = await server.inject({
        method: 'post',
        url: '/api/data/100',
        payload: 17,
      });
      expect(res.statusCode).to.equal(404);
    });
  });

  describe('next', () => {
    it('should return the next dataset index', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/api/data/1/next',
      });
      expect(res.result).to.equal(2);
    });
    it('should return the next dataset index looping to the front of the array', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/api/data/3/next',
      });
      expect(res.result).to.equal(1);
    });
  });
});
